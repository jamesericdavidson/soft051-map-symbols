# Map Symbols

Coursework for SOFT051 - Computer Programming

*Match the Ordnance Survey map symbols*

| [Play Map Symbols from the browser](https://jamesdavidson.xyz/demos/map-symbols/) |
| --------------------------------------------------------------------------------- |
| [![Screenshot demonstration of playing Map Symbols](demo1.png "A successful turn in Map Symbols")](https://jamesdavidson.xyz/demos/map-symbols/) |
| [![Screenshot demonstration of playing Map Symbols](demo2.png "An unsuccessful turn in Map Symbols")](https://jamesdavidson.xyz/demos/map-symbols/) |

---

* `Game_SMB.html` is the submitted version, requiring `<bgsound>` to play sounds under Internet Explorer

* `index.html` is the updated version, which works with modern browsers (as of June 2022):
    * Replaces `<bgsound>` with `<audio>`
    * Adds a missing parameter to `setAttribute`
